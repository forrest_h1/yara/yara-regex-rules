/* (Not so) simple Regex match
for MAC Addresses. 
Written by Forrest Hooker, 02/25/2024 */


rule find_MAC_addresses {
		strings:
			// Pretty sure YARA is using Perl Regex
			$macAddr = /([0-9a-fA-F]{2}[-:]){5}[0-9a-fA-F]{2}/
			
			// Uncomment for unique mac addresses
			//$bCast = /([fF]{2}[-:]){5}[fF]{2}/

		condition:
			$macAddr
			
			// Uncomment for unique mac addresses
			//$macAddr and not $bCast
}


