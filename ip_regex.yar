/* Simple rule to do regex matches for 
IPv4 Strings within files. 
Written by Forrest Hooker, 02/25/2024*/


rule find_ipv4_addresses {
    strings:
        //(Slightly broken) Regex pattern for IPv4 Addresses
		$ipv4_pattern = /[0-9]{1,3}(\.[0-9]{1,3}){3}/

    condition:
        $ipv4_pattern
}
