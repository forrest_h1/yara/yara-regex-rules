# YARA Regex Rules

This exists for fellow Malware Analysis students trying to figure out YARA's syntax, and how to perform regex matches (Which is something the professor didn't mention... yet). This is not a tutorial on Regular Expressions. I am not your best resource for Regex searches. 

These are exclusively written for YARA 4.1.3 on Linux (Debian/Ubuntu/Mint). They should work fine for the Yara64.exe we were given in class.

## Usage

`yara <YOUR_RULE_FILE.yar> <your_args_here> -s -w`

**OR**

`yara64.exe <YOUR_RULE_FILE.yar> <your_args_here> -s -w`

The `-w` flag just tells Yara to stop printing that terrible "hEy ReGeX iS SoMeTImeS KiNDa SloW LoL" message.

## ip_regex

Very simplified form of trying to find IP addresses within the `0.0.0.0`-`255.255.255.255` range. Could probably be better. 

## mac_regex

Matches MAC/PHYS addresses in either `XX-XX-XX...` or `XX:XX:XX` format. Works with upper + lowercase. Can also perform Broadcast (`FF:FF:FF...`) address searches, or to find unique MAC addresses.

## phone_regex

Tries to match the following 3 phone number formats:

- XXXXXXXXXX  (10-Digit Area Code + Prefix + Subscriber Line, no separator)
- XXX-XXX-XXXX (10-Digit Number **WITH** separators)
- (XXX) XXX-XXXX (10-Digit Number in the objectively terrible format I cannot stand)

**DOES NOT ACCOUNT FOR COUNTRY CODES.** I might add that later, but we haven't been given WhatsApp formatted phone numbers to play with yet.

## email_regex

*This is going to probably give you like 8 different matches for the same string*. This is because YARA seems to be based on Perl's Regex engine with not much additional proccessing over it. **You probably only care about the first result for most assignments.**

Looks for anything like: `user@domain.ext`

If these are helpful, I'll add more. If something seems wrong, please contact **MY UTSA EMAIL** with a screenshot of the error.

Enjoy.