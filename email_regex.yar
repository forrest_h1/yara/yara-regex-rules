/* Email Regex matching for Yara. Not perfect.
Written by Forrest Hooker, 02/26/2024 */

rule find_Email {
		strings:
			$emailAddr = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/

		condition:
			$emailAddr
}
