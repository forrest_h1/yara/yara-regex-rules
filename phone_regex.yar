/* Simple rule(s) for regex matching
United States telephone numbers
Written by Forrest Hooker, 02/25/2025 */

rule find_tel_num {
		strings:
			
			// Tries to match "XXXXXXXXXX"
			$baseNum = /[0-9]{10}/

			// Tries to match "XXX-XXX-XXXX"
			$sepNum = /([0-9]{3}[-]){2}[0-9]{4}/

			// Tries to match "(XXX) XXX-XXXX"	
			$webNum = /[(][0-9]{3}[)][ ][0-9]{3}[-][0-9]{4}/

		condition:
			$baseNum or $sepNum or $webNum
}
